--
-- File generated with SQLiteStudio v3.1.0 on mar oct 25 11:38:48 2016
--
-- Text encoding used: System
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: Persona
CREATE TABLE Persona (id_persona INTEGER PRIMARY KEY, nombre CHAR (50) NOT NULL, apellido_paterno CHAR (50) NOT NULL, apellido_materno CHAR (50) NOT NULL, run INTEGER (20) NOT NULL, digito_verificador VARCHAR (1) NOT NULL, direccion CHAR (100) NOT NULL, telefono VARCHAR (20) NOT NULL, mail CHAR (40) NOT NULL);
INSERT INTO Persona (id_persona, nombre, apellido_paterno, apellido_materno, run, digito_verificador, direccion, telefono, mail) VALUES (1, 'Juan', 'Perez', 'Lopez', 13369789, 'k', 'San Pablo 1820 ', '55545621', 'juan.perez@gmail.com');
INSERT INTO Persona (id_persona, nombre, apellido_paterno, apellido_materno, run, digito_verificador, direccion, telefono, mail) VALUES (2, 'Pablo', 'Lorca', 'Ruiz', 15478965, '0', 'santa lucia 240', '347894', 'pablo@gmail.com');
INSERT INTO Persona (id_persona, nombre, apellido_paterno, apellido_materno, run, digito_verificador, direccion, telefono, mail) VALUES (3, 'Pablo2', 'Lorca2', 'Ruiz2', 15478965, '0', 'santa lucia 245', '347894', 'pablo2@gmail.com');
INSERT INTO Persona (id_persona, nombre, apellido_paterno, apellido_materno, run, digito_verificador, direccion, telefono, mail) VALUES (4, 'Marco', 'Diaz', 'Valdivia', 14789546, '0', 'Las Bahamas', '4879587', 'marco@gmail.com');
INSERT INTO Persona (id_persona, nombre, apellido_paterno, apellido_materno, run, digito_verificador, direccion, telefono, mail) VALUES (5, 'ana', 'Paes', 'Arredondo', 14789456, 'k', 'Moneda 235', '5479852', 'ana@gmail.com');

-- Table: libros
CREATE TABLE libros (id_libro INTEGER, titulo CHAR (30), autor CHAR (20), editorial CHAR (30), paisociudad CHAR (30), anio DATE, dewey INTEGER, cutter VARCHAR (20), isbn INTEGER (20), PRIMARY KEY (id_libro));
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (1, 'Ensayos Politicos', 'David Hume', 'Tecnos', 'Espa�a', 1987, 32001, 'H921e', 8430913726);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (2, 'Representacion en Chile', 'Patricio Orellana', 'Senda', 'Chile', 2008, 32309, 'O66r', 978918643130);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (3, 'Tectos Politicos', 'Edmundo Burke', 'Fondo Cultura Economica', 'Mexico', 1984, 32001, 'B959t', 9681614860);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (4, 'El Concepto de Representacion', 'Hanna Fenichel Pitkin', 'Centro Estudios Politicos', 'Espa�a', 2014, 328, 'P684c', 9788425917970);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (5, 'El quiebre de la Democracia en Chile', 'Arturo Valenzuela', 'Flacso', 'Chile', 1989, 320983, 'B78', 9562050296);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (6, 'Tectos Politicos', 'Edmundo Burke', 'Fondo Cultura Economica', 'Mexico', 1984, 32001, 'B959t', 9681614860);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (7, 'El Concepto de Representacion', 'Hanna Fenichel Pitkin', 'Centro Estudios Politicos', 'Espa�a', 2014, 328, 'P684c', 9788425917970);
INSERT INTO libros (id_libro, titulo, autor, editorial, paisociudad, anio, dewey, cutter, isbn) VALUES (8, 'El quiebre de la Democracia en Chile', 'Arturo Valenzuela', 'Flacso', 'Chile', 1989, 320983, 'B78', 9562050296);

-- Table: Bitacora
CREATE TABLE Bitacora (id_bitacora INTEGER PRIMARY KEY NOT NULL, prestamos CHAR (50) NOT NULL, renovacion DATE (50) NOT NULL, devolucion DATE (50) NOT NULL);
INSERT INTO Bitacora (id_bitacora, prestamos, renovacion, devolucion) VALUES (1, 'Los Derechos de los otros', '20-10-2016', '27-10-2016');
INSERT INTO Bitacora (id_bitacora, prestamos, renovacion, devolucion) VALUES (2, 'El Chile Perplejo ', '20-10-2016', '27-10-2016');
INSERT INTO Bitacora (id_bitacora, prestamos, renovacion, devolucion) VALUES (3, 'Manual Ciencia Politica', '21-10-2016', '28-10-2016');
INSERT INTO Bitacora (id_bitacora, prestamos, renovacion, devolucion) VALUES (4, 'Ensayos Politicos', '21-10-2016', '28-10-2016');
INSERT INTO Bitacora (id_bitacora, prestamos, renovacion, devolucion) VALUES (5, 'La Represion en Chile', '21-10-2016', '28-10-2016');

-- Table: Circulacion
CREATE TABLE Circulacion (id_circulacion INTEGER NOT NULL PRIMARY KEY, id_libros INTEGER NOT NULL REFERENCES libros (id_libro), Manuales CHAR (50) NOT NULL, Diccionarios CHAR (50) NOT NULL);

COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
